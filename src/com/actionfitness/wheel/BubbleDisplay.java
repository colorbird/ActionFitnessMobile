package com.actionfitness.wheel;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

public class BubbleDisplay extends Activity {


	// openGL variables
	GLSurfaceView glBubbleSurface;
	float mmInPx=171.42f;

	//screen lock variables
	PowerManager powerManager;
	WakeLock wakeLock;
	
	//screen variables
	LinearLayout linearLayout;
	
	
	//debug variables
	private static final String TAG = "BubbleDisplay";
	private boolean debug=true;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//no header
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//instantiate to this class
		glBubbleSurface=new GLSurfaceView(this);
		//set the renderer
		glBubbleSurface.setRenderer(new CanvasRenderer(getBaseContext()));
		//setContentView(R.layout.);
		
		//setting the width and height of Glsurface
		// linearLayout=(LinearLayout)findViewById(R.id.screenlayout);
	    // linearLayout.setGravity(Gravity.CENTER);

	//set the GL surface overrides the screenlayout
         setContentView(glBubbleSurface);
		

		//make the screen unlocked
		 powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		 wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Lock");
		 
		 
		 if(debug)
			 
		 {
			 float mmInPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 20, 
		                getResources().getDisplayMetrics());//171.43
			Log.d(TAG, "Dimension Values" + mmInPx) ;
		 }
		 //set the width and height of opengl surface
		 ViewGroup.LayoutParams layoutParams=glBubbleSurface.getLayoutParams();
		 layoutParams.width=(int) mmInPx*3;
		 layoutParams.height=(int) mmInPx*5;
		
		 glBubbleSurface.setLayoutParams(layoutParams);
		
	}


	@Override
	
	protected void onPause() {
		super.onPause();
		glBubbleSurface.onPause();
		wakeLock.release();

	}

	@Override
	protected void onResume() {
		super.onResume();
		glBubbleSurface.onResume();
		wakeLock.acquire();
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
