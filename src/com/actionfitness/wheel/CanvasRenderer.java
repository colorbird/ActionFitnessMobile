package com.actionfitness.wheel;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

public class CanvasRenderer implements Renderer {


	//toiletbubble variables
	private ToiletBubble bubble;
	
	//toilet variables
	private Toilet toilet;
	
	
	
	
	public   CanvasRenderer(Context context){
		
		bubble=new ToiletBubble(context);
		toilet=new Toilet();
		
		
	}
	
	/*Renderer Methods*/
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {

		//disable dither and low quality and higher performance 
		gl.glDisable(GL10.GL_DITHER);
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);
		//background color rgba
		gl.glClearColor(0.4f, 0.8f, 1f, 1f);
		//background Depth
		gl.glClearDepthf(1f);
		
	}
	
	@Override
	public void onDrawFrame(GL10 gl) {
		
		//gl color buffer and depth buffer
	   gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT);
		
	   gl.glMatrixMode(GL10.GL_MODELVIEW);
	   gl.glLoadIdentity();
	   
	   //setting the camera
	   GLU.gluLookAt(gl, 0, 0, -6, 0, 0, 0, 0, 2, 0);
	   
	   //draw the seat
	   //toilet.drawSeat(gl);
	   //then draw the inner white toilet
	   toilet.draw(gl);
	  
	   
	   //first draw water
	   bubble.drawWater(gl);

       //draw bubble
	   bubble.draw(gl);
	  
	}
	
	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		
		//bottom left is origin in opengl
		gl.glViewport(0, 0, width, height);
		
		//to keep the ratio same irrespective of orientation 
		//change the multiplier for adjusting the view
		float ratio=(float)width/height ;
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
			
		//set up the gl view relative to the screen
		gl.glFrustumf(-ratio, ratio, -1, 1, 1, 25);
		
		
	}

	

}
