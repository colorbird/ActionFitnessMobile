package com.actionfitness.wheel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar.LayoutParams;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionfitness.R;
import com.actionfitness.graphing.RealtimeChartSurfaceView;
import com.actionfitness.rings.PlayActivity;
import com.actionfitness.rings.ResultsActivity;

public class WheelActivity extends Activity {

	//startbutton variables
	static  boolean startButtonClicked=false ;
	// openGL variables
		GLSurfaceView glBubbleSurface;
	
	// Operational variables
	private boolean activityrunning = true;
	private boolean running = true;
	boolean centerSet = false;
	boolean overflowed = false;

	// UI elements
	static LinearLayout fillLayout = null;
	static TextView percentFullBox = null;

	static TextView angleBox = null;
	static TextView hintBox = null;

	static LinearLayout angleGraphLayout = null;
	static Button startStopButton = null;

	// Chart objects
	private RealtimeChartSurfaceView angleChart = null;

	// Angle reading variables
	static public double latestAngleX = -1;
	static public double latestAngleY = -1;
	static public double latestAngleZ = -1;
	static public double latestAngleInUse = -1;

	// Graphing variables
	public static double currentPercent = 0;
	private double centerOffset = 0;
	private double currentOffset = 0;
	private long startTime = 0;
	public static long runningTime = 0;

	// Data variables
	float[] angleData = new float[600];
	public static List<Double> angleList = new ArrayList<Double>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_wheel);
		
		for (int i = 0; i < 600; i++) {
			angleData[i] = 0;
		}
		
		
		
		// Get layout objects
		fillLayout = (LinearLayout) findViewById(R.id.fillLayout);
		percentFullBox = (TextView) findViewById(R.id.statusBox);
		angleBox = (TextView) findViewById(R.id.angleBox);
		hintBox = (TextView) findViewById(R.id.hintBox);

		angleGraphLayout = (LinearLayout) findViewById(R.id.angleGraphLayout);
		startStopButton = (Button) findViewById(R.id.start_stop_button);
				
		
		glBubbleSurface=new GLSurfaceView(this);
		//set the renderer
		glBubbleSurface.setRenderer(new CanvasRenderer(getBaseContext()));
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		fillLayout.addView(glBubbleSurface, params);
		
		// Add angle chart to the screen
		angleChart = new RealtimeChartSurfaceView(this);
		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		angleGraphLayout.addView(angleChart, params1);
		angleChart.setChartData(angleData);
		

		// Draw status information
		percentFullBox.setText("Bucket: "
				+ ((double) Math.round(currentPercent * 10) / 10) + "% | "
				+ "Time: " + ((double) Math.round(runningTime / 100) / 10)
				+ " s");

		// Write text on button
		if (centerSet) {
			if (running) {
				startStopButton.setText("Stop"); // This can never be the case
													// at this point of the code
			} else {
				startStopButton.setText("View Results");
			}
		} else {
			startStopButton.setText("Start");
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		activityrunning = false;
		//finish();
		

	}


	// Start the main thread
	@Override
	public void onResume() {
		super.onResume();

		activityrunning = true;

		// Main loop thread
		Thread mainLoopThread = new Thread() {
			public void run() {
				while (running) {
					WheelActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {

							// Determine which angle to use based on orientation

							latestAngleInUse = ToiletBubble.score;

							// Update latest values to text boxes
							angleBox.setText("Angle: " + latestAngleInUse);

							// Calculate the current offset from center
							currentOffset = ((double) (latestAngleInUse + 90) / 180)
									* 100 - centerOffset;

							if (currentOffset == 0) {
								hintBox.setText("******");
							} else {
								hintBox.setText("");
							}
							// Display current offset or fill amount as
							// necessary
							if (centerSet) {
								// Increase fill amount as required
								currentPercent += Math.abs(currentOffset * 0.1);

								// Check for overflow
								if (currentPercent >= 100) {
									currentPercent = 100;
								}

								// Save latest data to lists and logs
								angleList.add(currentPercent);
								// appendLog("Angle " + angleList.size() +
								// " value: " + angleList.get(angleList.size() -
								// 1));

								// Update status information (percent full, time
								// elapsed)
								runningTime = SystemClock.elapsedRealtime()
										- startTime;
								percentFullBox.setText("Bucket: "
										+ ((double) Math
												.round(currentPercent * 10) / 10)
										+ "% | "
										+ "Time: "
										+ ((double) Math
												.round(runningTime / 100) / 10)
										+ " s");

								// Check if the user has lost
								if (currentPercent == 100 && !overflowed) {
									overflowed = true;
									running = false;
									startStopButton.setText("View Results");
									if (activityrunning) {
										overflowPopup();
									}
								}
							} else {
								// Display current offset
								// currentPercent = ((double)(latestAngleInUse +
								// 90) / 180) * 100;
							}

							// Push back all data in the angle graph array
							for (int i = 0; i < 599; i++) {
								angleData[i] = angleData[i + 1];
								//Log.d("WheelActivity", angleData[i]
									//	+ " angledata");
							}

							// Update angle graph
							angleData[599] = (float) latestAngleInUse;
							angleChart.setChartData(angleData);

						}
					});

					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		};

		mainLoopThread.start();
	}

	// When the start/stop button is pressed
	public void startStopPressed(View v) {
		// Check if the game has started already, if so, end it or move to
		// results activity
		if (centerSet) {
			// Check if the game has ended, if not, end it
			if (running) {
				running = false;
				startStopButton.setText("View Results");
				startButtonClicked=false;
				// If so, switch to results activity
			} else {
				Intent switchActivity = new Intent(this, WheelResultsActivity.class);
				startActivity(switchActivity);
				// finish();
			}
			// If not, start the game
		} else {
			centerOffset = currentOffset;
			currentPercent = 0;
			centerSet = true;
			startStopButton.setText("Stop");
			startTime = SystemClock.elapsedRealtime(); // Begin timer
			//add code for resetting the center
			startButtonClicked=true;
			//ToiletBubble.offsetNotSet=true;
			//ToiletBubble.score=0;
			Log.d("WheelActivity","startbutton value: "+ startButtonClicked );
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.play, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_help) {
			helpPopup();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// Arduino communication issue pop-up box
	public void helpPopup() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Help")
				.setMessage(
						"Setting Up The Cross-Bar:\n"
								+ "- A horizontal bar attached with a rotating joint to a vertical bar\n"
								+ "- A ring hanging from either end of the horizontal bar\n"
								+ "- Attach device to the horizontal bar such that it rotates with the bar\n"
								+ "- Suspend yourself on the rings (doing a pull-up or other such manoeuvre) and start using the app\n\n"
								+ "More information on actional fitness (including pictures of the cross-bar apparatus): http://wearcam.org/mannfit/mannfit.pdf")
				.setPositiveButton("OK", null);

		builder.create().show();
	}

	// Player lost pop-up box
	public void overflowPopup() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("You've Lost!")
				.setMessage("The bucket has overflowed!")
				.setPositiveButton("OK", null);

		builder.create().show();

	}

	// Saves data to log files
	public void appendLog(String text) {
		File logFile = new File("sdcard/actionFitnessLog.txt");
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				Log.d("error", "Error: unable to create log file.");
				e.printStackTrace();
			}
		}

		try {
			// BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			Log.d("error", "Error: unable to write data to log file.");
			e.printStackTrace();
		}
	}
}
