package com.actionfitness.wheel;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;


public class ToiletBubble {
	
	//public Boolean 
	static Boolean offsetNotSet=true;
	//circle variables
		int circlePoints=30;
		static float radius=0.2f;
		static float center_x=0.0f;
		static float center_y=0.0f;
		//outer vertices of the circle i.e. excluding the center_x,center_y
	    int circumferencePoints=circlePoints-1;
		//circle vertices and buffer variables 
		int vertices=0;
		float circleVertices[]= new float[circlePoints*2]; 
		private FloatBuffer bubbleBuff; //4bytes per float
		
		//water circle variables
		float waterRadius=0.2f;
		//outer vertices of the circle i.e. excluding the center_x,center_y
		//circle vertices and buffer variables 
		int waterVertices=0;
		float waterCircleVertices[]= new float[circlePoints*2]; 
		private FloatBuffer waterBuff; //4bytes per float
		
		
		//sensor variables
		private SensorManager sManager;
		private Sensor sensor ;
	   static float sensor_x=0f,sensor_y=0f;//,sensor_z=0f;
		static float sensor_offsetx=0f,sensor_offsety=0f;//,sensor_z=0f;

		//score variables
		public static float score=0;
		public static float calibrateScore=2;
		private float scoreThreshold=0.5f;
		private float waterIncrease=0.001f;
		
	   //Toilet class variables;
		Toilet toilet; 
		float  temp_x;
		float temp_y;
		//debug variables
		private static final String TAG = "ToiletBubble";
        private boolean debug=true;
        
        
        

    private float rgbaValues[]={
    		0
    };
    private FloatBuffer colorBuff;
    
    
	//set up the buffers
	public ToiletBubble(Context context){
		
		
		//the initial buffer values
   	   circleVertices[vertices++] = center_x;
   	   circleVertices[vertices++] = center_y;
   	   
   	   //water values
    	waterCircleVertices[waterVertices++] = center_x;
        waterCircleVertices[waterVertices++] = center_y;
       
        
   	 //set circle vertices values 
  		for (int i = 0; i < circumferencePoints; i++)
  		{
      	
  			float percent = (i / (float) (circumferencePoints - 1));
  			float radians = (float) (percent * 2 * Math.PI);

  			//bubble vertex position
  			float outer_x = (float) (center_x + radius * Math.cos(radians));
  			float outer_y = (float) (center_y + radius * Math.sin(radians));

  			circleVertices[vertices++] = outer_x;
  			circleVertices[vertices++] = outer_y;
  			
  			
  			//water vertex position
  			float waterOuter_x = (float) (center_x + waterRadius * Math.cos(radians));
  			float waterOuter_y = (float) (center_y + waterRadius * Math.sin(radians));

  			waterCircleVertices[waterVertices++] = waterOuter_x;
  			waterCircleVertices[waterVertices++] = waterOuter_y;
  		}   
		
   	  
		
		// float buffer short has 4 bytes
		ByteBuffer toiletByteBuff = ByteBuffer
				.allocateDirect(circleVertices.length * 4);
		// garbage collector wont throw this away
		toiletByteBuff.order(ByteOrder.nativeOrder());
		bubbleBuff = toiletByteBuff.asFloatBuffer();
		bubbleBuff.put(circleVertices);
		bubbleBuff.position(0);
		
		
		// float buffer short has 4 bytes
				ByteBuffer waterByteBuff = ByteBuffer
						.allocateDirect(waterCircleVertices.length * 4);
				// garbage collector wont throw this away
				waterByteBuff.order(ByteOrder.nativeOrder());
				waterBuff = waterByteBuff.asFloatBuffer();
				waterBuff.put(waterCircleVertices);
				waterBuff.position(0);

		// float buffer short has 4 bytes
				ByteBuffer clrBuff = ByteBuffer.allocateDirect(rgbaValues.length * 4);
				// garbage collector wont throw this away
				clrBuff.order(ByteOrder.nativeOrder());
				colorBuff = clrBuff.asFloatBuffer();
				colorBuff.put(rgbaValues);
				colorBuff.position(0);
			
				//get a hook to the sensor service  
		        sManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE); 
		        sensor = sManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		        if (sensor != null) {
		        	sManager.registerListener(mySensorEventListener, sensor,
		              SensorManager.SENSOR_DELAY_NORMAL);
		        }
		        
      //initialise toilet
		   toilet= new Toilet();
		   
		
		
	}
	public void changeWaterRadius(){
		
		int waterVertices=0;
      
		if((score>scoreThreshold) && waterRadius<toilet.radius )
		{
			waterRadius=waterRadius+waterIncrease;
		}
		
		 //water values
    	waterCircleVertices[waterVertices++] = center_x;
        waterCircleVertices[waterVertices++] = center_y;
        
        //set circle vertices values 
  		for (int i = 0; i < circumferencePoints; i++)
  		{
      	
  			float percent = (i / (float) (circumferencePoints - 1));
  			float radians = (float) (percent * 2 * Math.PI);

  			//water vertex position
  			float waterOuter_x = (float) (center_x + waterRadius * Math.cos(radians));
  			float waterOuter_y = (float) (center_y + waterRadius * Math.sin(radians));

  			waterCircleVertices[waterVertices++] = waterOuter_x;
  			waterCircleVertices[waterVertices++] = waterOuter_y;
  		}   
		
		// float buffer short has 4 bytes
				ByteBuffer waterByteBuff = ByteBuffer
						.allocateDirect(waterCircleVertices.length * 4);
				// garbage collector wont throw this away
				waterByteBuff.order(ByteOrder.nativeOrder());
				waterBuff = waterByteBuff.asFloatBuffer();
				waterBuff.put(waterCircleVertices);
				waterBuff.position(0);

		
  		 
  	 }
	//draw method for bubble
	public void draw(GL10 gl){
		

 		//get the front face
 				gl.glFrontFace(GL10.GL_CW); //front facing is clockwise 
 				gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
 				
 				//enable color array
 				gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
 				
 				//pointer to the buffer
 				gl.glVertexPointer(2, GL10.GL_FLOAT, 0, bubbleBuff);
 				
 				//pointer to color
 				gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuff);
 				
 				 //move the bubble
 				//if(debug)
 			  // Log.d(TAG,"Sensor Values x: " + sensor_x + " sensor values y: " + sensor_y);
 				
 				
 				
 				gl.glTranslatef(sensor_x, sensor_y, 0f);
 				//draw hollow circle
 				//gl.glDrawArrays(GL10.GL_LINE_LOOP, 1, circumferencePoints);
                
 				//draw circle as filled shape
 				gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, circlePoints);
 				
 				gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
 				gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
 				
 	
		
	}
	
	// draw method for water filling
	public void drawWater(GL10 gl) {
		 //allow water to change 
		if(WheelActivity.startButtonClicked)
		{
		   changeWaterRadius();
		}
		// get the front face
		gl.glFrontFace(GL10.GL_CW); // front facing is clockwise
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

		// enable color array
		gl.glEnableClientState(GL10.GL_COLOR_ARRAY);

		// pointer to the buffer
		gl.glVertexPointer(2, GL10.GL_FLOAT, 0, waterBuff);

		// pointer to color
		gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuff);

		// move the bubble
		//if (debug)
			// Log.d(TAG,"Sensor Values x: " + sensor_x + " sensor values y: " +
			// sensor_y);

			//gl.glTranslatef(sensor_x, sensor_y, 0f);
		// draw hollow circle
		// gl.glDrawArrays(GL10.GL_LINE_LOOP, 1, circumferencePoints);

		// draw circle as filled shape
		gl.glDrawArrays(GL10.GL_TRIANGLE_FAN, 0, circlePoints);

		gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

	}

	 private SensorEventListener mySensorEventListener = new SensorEventListener() {
	
	/*Sensor methods*/
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		
		//if sensor is unreliable, return void  
       if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE)  
       {  
           return;  
       }  
       if(WheelActivity.startButtonClicked)
       {//sensor data
       sensor_x=event.values[2]/4;//roll
       sensor_y=event.values[1]/4;//pitch
      // sensor_z=event.values[0]/2;//yaw
       }
       
       
       //center the bubble when started
       if(WheelActivity.startButtonClicked && offsetNotSet)
       {
    	   
    	   sensor_offsetx=sensor_x;
    	   sensor_offsety=sensor_y;
    	   
           
           Log.d(TAG,"sensor x and sensor y:  "+sensor_offsetx+sensor_offsety);
           offsetNotSet=false;
       }
        sensor_x-=sensor_offsetx;
        sensor_y-=sensor_offsety;
        
        
	     //make the bubble stay inside the toilet x2+y2>r2 x=x/sqrt(x2+y2)
	       if(((sensor_x*sensor_x+sensor_y*sensor_y)>(toilet.radius*toilet.radius)))
	       {
	    	  temp_x=((toilet.radius*sensor_x)/( (float) Math.sqrt(sensor_x*sensor_x+sensor_y*sensor_y)));
	    	  temp_y= ((toilet.radius*sensor_y)/((float) Math.sqrt(sensor_x*sensor_x+sensor_y*sensor_y)));
	    	 sensor_x=temp_x;
	    	 sensor_y=temp_y;
	    	
	       }
	   
	     //set the score
			score=(float) Math.sqrt(sensor_x*sensor_x+sensor_y*sensor_y)*calibrateScore;
           if(debug)
           {
       		Log.d(TAG,"Score :" +score);

           }
	}

	 };
	
	
}
