package com.actionfitness.rings;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class FillDrawer extends View {

	// Edge boundaries for the fill region
	
	private static final int topMargin = 0;

	// Paints
	Paint fillPaint = new Paint();
	
	// Height of fill to draw
	double percentFilled = 0;
	
	// Constructor
	public FillDrawer(Context context, double percent) {
		super(context);
		
		// Initialize fill paint 
		fillPaint.setColor(Color.parseColor("#66ccff"));//0099cc //9cc7f2 //ebf4fa /40a4df//66ccff
		
		// Get height
		percentFilled = percent;
	}

	// Draw the fill during the draw method
	@Override
	public void onDraw(Canvas c) {
		// Get canvas dimensions
		int width = c.getWidth();
		int height = c.getHeight();
		
		double scaledHeightToDraw = height - (height - topMargin) * (percentFilled / 100);
		
		// Draw fill box
		c.drawRect(0, Math.round(scaledHeightToDraw), width, height, fillPaint);
	}
}
