package com.actionfitness.menu;

import com.actionfitness.R;
import com.actionfitness.R.drawable;
import com.actionfitness.R.id;
import com.actionfitness.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.view.Window;
import android.view.WindowManager;
public class HomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_home);
	}
	
	public void onClick(View v) 
	{
        int id = v.getId();
        switch (id) {
        case R.id.buttonGetStarted:{
        	//v.setBackgroundResource(R.drawable.exercisemouseover);

        	Intent intent = new Intent(this, GameMenuActivity.class);
        	startActivity(intent);
        	break;
        }
        }
	}
	
	
}
